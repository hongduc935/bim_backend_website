import dotenv from 'dotenv'
import express from 'express'
import morgan from 'morgan'
import bodyParser from "body-parser"
import cors from 'cors'
import compression from 'compression'
import helmet from "helmet"
import ConnectDB from "./config/ConnectDB"
import ErrorHandle from "./middleware/ErrorHandle"
import indexRoutes from './router/indexRoutes'
import axios from 'axios'
dotenv.config()
const app = express()

app.use(bodyParser.urlencoded({ extended: true }))

import http from 'http'


// ExpressPeerServer(server, { path: "/" })

const corsOptions = {
    methods: ['GET', 'POST', 'DELETE', 'UPDATE', 'PUT', 'PATCH'],
    "origin": "*",
    "preflightContinue": false,
    "optionsSuccessStatus": 204
};

//middleware
// app.use(cors(corsOptions));
app.use(cors());
app.use(morgan('dev'))
app.use(express.json());
app.use(express.urlencoded())
app.use(compression({
    level: 6,
    threshold: 100 * 1000
}))
app.use(helmet())

app.use('/api/v1/', indexRoutes)

const replyMessage = async (userId, text) => {
    const accessToken = 'wbdb1cIeTdlfMUmFPgjmSkzVwGrYjZS3dJFT0XkJ7WN_2za_ABmXAzyNmJGAbG0Svsll9GwPBb7R4yvN1-OIKl0OmMGyhIjSvZEmKGw-OodP584C5RHJ1lq2pNvwXXvvytZjPGxG41lJVVGzGCqxARP1f2HontyfYMoQCbt0L12kS8uDViyTBePouYP--XCakLcAFcN4VXwqKuSONQ9K68yNb3voXtSV-mI8FmovUIc7BxyGMwnvRQiSW494drjge22sJKcENdcnBBrTEv8XKyOa-tudeZfJznFWQIQk7KEe2ezzVQfx78S8_JzKjXucf0N28MgC90Ar1VaHEAOL7iqOyM8oXYXgzWZXL2ciBK7mByqxTePBGa--nwjCOB1_Sm';  // Thay bằng access token của bạn

    const messageData = {
        recipient: {
            user_id: userId
        },
        message: {
            text: text
        }
    };

    try {
        await axios.post('https://openapi.zalo.me/v2.0/oa/message', messageData, {
            headers: {
                'Content-Type': 'application/json',
                'access_token': accessToken
            }
        });
        console.log('Đã phản hồi tin nhắn');
    } catch (error) {
        console.error('Lỗi khi phản hồi tin nhắn:', error);
    }
};

app.post('/api/v1/webhook', (req, res) => {
    const event = req.body;

    if (event.event_name === 'user_send_text') {
        const userId = event.sender.id;
        const userMessage = event.message.text;

        console.log(`Người dùng ${userId} gửi tin nhắn: ${userMessage}`);

        // Phản hồi lại người dùng
        replyMessage(userId, 'Cảm ơn bạn đã liên hệ với chúng tôi!');
    }

    res.status(200).send('OK');
});
app.all("*")
app.use(ErrorHandle)

ConnectDB().then(() => {
    console.log("")
})

let server = http.createServer(app)

export default server

