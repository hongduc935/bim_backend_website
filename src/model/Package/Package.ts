import mongoose from "mongoose"

const PackageSchema = new mongoose.Schema({
    name_package: {
        type: String,
        required: true,
    },
    price_package:{
        type: Number,
        required: true,
    },
    date_package:{
        type: Number,
        required: true,
    },
}, { timestamps: true }
)
const PackageModels = mongoose.model('packages', PackageSchema)
export default PackageModels

