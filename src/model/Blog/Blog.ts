import mongoose from "mongoose"

const BlogSchema = new mongoose.Schema({
    blog_title: {
        type: String,
        required: true,
    },
    blog_short_content:{
        type: String,
        required: true,
    },
    blog_main_image:{
        type: String,
        required: true,
    },
    blog_content:{
        type: String,
        required: true,
    },
    slug:{
        type: String,
        required: true,
        unique:true
    }
}, { timestamps: true }
)
const BlogModels = mongoose.model('blogs', BlogSchema)
export default BlogModels
