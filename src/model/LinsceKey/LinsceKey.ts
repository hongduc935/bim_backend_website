import mongoose, { model } from "mongoose"
// const { ObjectId } = mongoose.Schema.Types

const LinsceSchema = new mongoose.Schema({
    email: {
        type: String,
        required: true,
        unique: true
    },
    phone:{
        type: String,
        required: true,
        unique: true
    },
    serial:{
        type: String,
    },
    name:{
        type: String,
        required: true,
    },
    keyHash:{
        type: String,
        required: true,
        unique: true
    },
    exprire:{
        type: String,
        required: true,
    },
    status:{
        type: Boolean,
        required: true,
    },
}, { timestamps: true }
)
const LinsceModels = mongoose.model('linsces', LinsceSchema)
export default LinsceModels
// module.exports = LinsceModels

