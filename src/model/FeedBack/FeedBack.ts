import mongoose from "mongoose"

const FeedBackSchema = new mongoose.Schema({
    name: {
        type: String,
        required: true,
    },
    email:{
        type: String,
        required: true,
    },
    content:{
        type: String,
        required: true,
    }
}, { timestamps: true }
)
const FeedBackModels = mongoose.model('feedbacks', FeedBackSchema)
export default FeedBackModels

