import mongoose, { model } from "mongoose"

const PostLPSchema = new mongoose.Schema({
    title: {
        type: String,
        required: true,
    },
    short_content:{
        type: String,
        required: true,
    },
    link:{
        type: String,
        required: true,
    },
    description:{
        type: String,
        required: true,
    }
}, { timestamps: true }
)
const PostLPModels = mongoose.model('post_lp', PostLPSchema)
export default PostLPModels
// module.exports = LinsceModels

