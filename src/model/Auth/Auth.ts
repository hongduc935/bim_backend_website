import mongoose from "mongoose"

const AuthSchema = new mongoose.Schema({
    name: {
        type: String,
        required: true,
        trim: true
    },
    email: {
        type: String,
        required: true,
        unique: true
    },
    role: {
        type: String,
        enum: ["user","manage", "admin"],
        default: "user"
    },
    password: {
        type: String,
        required: true,
        minlength: [6, "Minlength least 6 character.."]
    },
    active: {
        type: Boolean,
        default: false
    },
    phone: {
        type: String, required: true,
        minlength: [10, "Minlength least 10 character.."],
        maxLength: [11, "Maxleng Phone 11 character"],
        unique: true
    },
    addressCurrent: {
        type: String,
        default: null
    }, // địa chỉ hiện tại
    addressHouse: {
        type: String,
        default: null
    },
    avatar: {
        type: Object,
        default: {
            url: 'https://res.cloudinary.com/dehtpa6ba/image/upload/v1677038569/manage_lib/avatar_wgaep7.webp',
            public_id: null
        }
    },
    list_notification:[{// danh sách id notification
        type:String,
        ref:"notification"
    }]
}, { timestamps: true }
)
const AuthModels = mongoose.model('auth', AuthSchema)
export default AuthModels
// module.exports = AuthModels

