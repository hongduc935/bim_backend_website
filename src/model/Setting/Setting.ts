import mongoose, { model } from "mongoose"

const SettingSchema = new mongoose.Schema({
    total_download: {
        type: Number,
        required: true,
    }
}, { timestamps: true }
)
const SettingModels = mongoose.model('setting', SettingSchema)
export default SettingModels
// module.exports = LinsceModels

