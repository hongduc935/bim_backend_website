import mongoose, { model } from "mongoose"
// const { ObjectId } = mongoose.Schema.Types

const NotificationSchema = new mongoose.Schema({
    title:{
        type: String,
        required: true,
    },
    content:{
        type: String,
        required: true,
    },
    type:{ // loại gửi 
        type:String,
        enum:["version","exprire","extends"]
    },
    list_user: [{ // danh sách email user
        type: String,
    }],
}, { timestamps: true }
)
const NotificatioModels = mongoose.model('notification', NotificationSchema)
export default NotificatioModels
// module.exports = LinsceModels

