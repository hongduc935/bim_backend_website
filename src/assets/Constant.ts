import {convertDateDDMMMMYYYY} from '../utils/Datetime'

export const TemplateExprireNotificationEmail = (data:any)=>{
    let string = 
    ` 
                <table role="presentation" style="width:100%;border-collapse:collapse;border:0;border-spacing:0;background:#ffffff;">
                <tr>
                  <td align="center" style="padding:0;">
                    <table role="presentation" style="width:602px;border-collapse:collapse;border:1px solid #cccccc;border-spacing:0;text-align:left;">
                      <tr>
                        <td align="center" style="padding:40px 0 30px 0;background:#70bbd9;">
                          <p style="text-align: center; font-size: 22px; font-weight: 600; color: #ffffff;">HQL - REVIT MEP PLUGINS</p>
                          <!-- <img src="https://assets.codepen.io/210284/h1.png" alt="" width="300" style="height:auto;display:block;" /> -->
                        </td>
                      </tr>
                      <tr>
                        <td style="padding:36px 30px 42px 30px;">
                          <table role="presentation" style="width:100%;border-collapse:collapse;border:0;border-spacing:0;">
                            <tr>
                              <td style="padding:0 0 36px 0;color:#153643;">
                                <p>Xin chào ${data.name}</p>
                                <h1 style="font-size:24px;margin:0 0 20px 0;font-family:Arial,sans-serif;">${data.title}  </h1>
                                <p style="margin:0 0 12px 0;font-size:16px;line-height:24px;font-family:Arial,sans-serif;">Nội dung cập nhật : ${data.content} </p>
                                <p style="margin:0 0 12px 0;font-size:16px;line-height:24px;font-family:Arial,sans-serif;">Ngày cập nhật : ${convertDateDDMMMMYYYY(data.start)} </p>
                                <p style="margin:0 0 12px 0;font-size:16px;line-height:24px;font-family:Arial,sans-serif;">Ngày hết hạn :${convertDateDDMMMMYYYY(data.end)}</p>
                                <p style="margin:0 0 12px 0;font-size:16px;line-height:24px;font-family:Arial,sans-serif;">Cảm ơn bạn đã tin tưởng và sử dụng dịch vụ của <b> HQL - Revit Mep Plugins</b></p>
                                <p style="margin:0 0 12px 0;font-size:18px;line-height:24px;font-family:Arial,sans-serif; font-weight: 700; color: #3379BC;">Best regards,</p>
                              </td>
                            </tr>
                          
                          </table>
                        </td>
                      </tr>
                      <tr>
                        <td style="padding:30px;background:#ee4c50;">
                          <table role="presentation" style="width:100%;font-size:9px;font-family:Arial,sans-serif;">
                            <tr>
                              <td style="padding:0;width:50%; color: #ffffff;" align="left">
                                  Fanpage: https://www.facebook.com/bimsmart.hql <br /><br />
                                  Hotline: 0987 904 586
                              </td>
      
                            </tr>
                          </table>
                        </td>
                      </tr>
                    </table>
                  </td>
                </tr>
              </table>
            `
    return string
}
export const TemplateUpdateVersionNotificationEmail = (data:any)=>{
  let string = 
  ` 
              <table role="presentation" style="width:100%;border-collapse:collapse;border:0;border-spacing:0;background:#ffffff;">
              <tr>
                <td align="center" style="padding:0;">
                  <table role="presentation" style="width:602px;border-collapse:collapse;border:1px solid #cccccc;border-spacing:0;text-align:left;">
                    <tr>
                      <td align="center" style="padding:40px 0 30px 0;background:#70bbd9;">
                        <p style="text-align: center; font-size: 22px; font-weight: 600; color: #ffffff;">HQL - REVIT MEP PLUGINS</p>
                        <!-- <img src="https://assets.codepen.io/210284/h1.png" alt="" width="300" style="height:auto;display:block;" /> -->
                      </td>
                    </tr>
                    <tr>
                      <td style="padding:36px 30px 42px 30px;">
                        <table role="presentation" style="width:100%;border-collapse:collapse;border:0;border-spacing:0;">
                          <tr>
                            <td style="padding:0 0 36px 0;color:#153643;">
                              <p>Xin chào ${data.name}</p>
                              <h1 style="font-size:24px;margin:0 0 20px 0;font-family:Arial,sans-serif;">${data.title}  </h1>
                              <p style="margin:0 0 12px 0;font-size:16px;line-height:24px;font-family:Arial,sans-serif;">Nội dung cập nhật : <br/></p>`+data.content+ `
                              <p style="margin:0 0 12px 0;font-size:16px;line-height:24px;font-family:Arial,sans-serif;">Ngày cập nhật : ${convertDateDDMMMMYYYY(data.start)} </p>
                              <p style="margin:0 0 12px 0;font-size:16px;line-height:24px;font-family:Arial,sans-serif;">Ngày hết hạn : ${convertDateDDMMMMYYYY(data.end)}</p>
                              <p style="margin:0 0 12px 0;font-size:16px;line-height:24px;font-family:Arial,sans-serif;">Link cập nhật version :<a href=${data.link_update}>${data.link_update}</a></p>
                              <p style="margin:0 0 12px 0;font-size:16px;line-height:24px;font-family:Arial,sans-serif;">Cảm ơn bạn đã tin tưởng và sử dụng dịch vụ của <b> HQL - Revit Mep Plugins</b></p>
                              <p style="margin:0 0 12px 0;font-size:18px;line-height:24px;font-family:Arial,sans-serif; font-weight: 700; color: #3379BC;">Best regards,</p>
                            </td>
                          </tr>
                        
                        </table>
                      </td>
                    </tr>
                    <tr>
                      <td style="padding:30px;background:#ee4c50;">
                        <table role="presentation" style="width:100%;font-size:9px;font-family:Arial,sans-serif;">
                          <tr>
                            <td style="padding:0;width:50%; color: #ffffff;" align="left">
                                Fanpage: https://www.facebook.com/bimsmart.hql <br /><br />
                                Hotline: 0987 904 586
                            </td>
    
                          </tr>
                        </table>
                      </td>
                    </tr>
                  </table>
                </td>
              </tr>
            </table>
          `
  return string
}

export const PlanPayment:any = {
  "pay30":175000,
  "pay60":10000,
  "pay180":499000,
  "pay360":799000,
  "pay720":1599000,
  "pay1080":1299000,
}

export const TemplateSendLinscekeyRegister = (data:any)=>{
    let string = 
    ` 
    <table role="presentation" style="width:100%;border-collapse:collapse;border:0;border-spacing:0;background:#ffffff;">
    <tr>
      <td align="center" style="padding:0;">
        <table role="presentation" style="width:602px;border-collapse:collapse;border:1px solid #cccccc;border-spacing:0;text-align:left;">
          <tr>
            <td align="center" style="padding:40px 0 30px 0;background:#70bbd9;">
              <p style="text-align: center; font-size: 22px; font-weight: 600; color: #ffffff;">HQL - REVIT MEP PLUGINS</p>
              <!-- <img src="https://assets.codepen.io/210284/h1.png" alt="" width="300" style="height:auto;display:block;" /> -->
            </td>
          </tr>
          <tr>
            <td style="padding:36px 30px 42px 30px;">
              <table role="presentation" style="width:100%;border-collapse:collapse;border:0;border-spacing:0;">
                <tr>
                  <td style="padding:0 0 36px 0;color:#153643;">
                    <p>Xin chào ${data.name_user}</p>
                    <h1 style="font-size:24px;margin:0 0 20px 0;font-family:Arial,sans-serif;">Linsce Key HqlTools để bạn kích hoạt </h1>
                    <p style="margin:0 0 12px 0;font-size:16px;line-height:24px;font-family:Arial,sans-serif;">Thông tin đơn hàng bạn đã đăng ký như sau:</p>
                    <!--<p style="margin:0 0 12px 0;font-size:16px;line-height:24px;font-family:Arial,sans-serif;">Mã kích hoạt : ${data?.keyHash}</p>-->
                 
                      <p style="margin:0 0 12px 0;font-size:16px;line-height:24px;font-family:Arial,sans-serif;">Mã kích hoạt:</p>
                     <div style="border: 1px dashed #3498db; padding: 10px; margin: 0 0 12px 0; font-size: 16px; line-height: 24px; font-family: Arial, sans-serif; background-color: #3498db; color: #fff; display: inline-block;">
        <p style="margin: 0;"> ${data?.keyHash}</p>
    </div>
                    <!-- <p style="margin:0 0 12px 0;font-size:16px;line-height:24px;font-family:Arial,sans-serif;">Tên gói : Gói ${data?.package} ngày</p>-->
                    <p style="margin:0 0 12px 0;font-size:16px;line-height:24px;font-family:Arial,sans-serif;">Tên gói : Gói .......</p>
                    <p style="margin:0 0 12px 0;font-size:16px;line-height:24px;font-family:Arial,sans-serif;">Đơn giá :${PlanPayment["pay"+data?.package] } ( VND ).</p>
                    <p style="margin:0 0 12px 0;font-size:16px;line-height:24px;font-family:Arial,sans-serif;">Ngày bắt đầu :${convertDateDDMMMMYYYY(data?.start)} </p>
                        <!-- <p style="margin:0 0 12px 0;font-size:16px;line-height:24px;font-family:Arial,sans-serif;">Ngày bắt đầu : ${convertDateDDMMMMYYYY(data?.start)} </p>-->
                    <p style="margin:0 0 12px 0;font-size:16px;line-height:24px;font-family:Arial,sans-serif;">Ngày hết hạn : ${convertDateDDMMMMYYYY(data?.end)}</p>
                    <!-- <p style="margin:0 0 12px 0;font-size:16px;line-height:24px;font-family:Arial,sans-serif;">Ngày hết hạn : ${convertDateDDMMMMYYYY(data?.end)} </p>-->
                    ${PlanPayment["pay"+data?.package] > 0 ?`<p style="margin:0 0 12px 0;font-size:16px;line-height:24px;font-family:Arial,sans-serif;">`+"Bạn vui lòng liên hệ anh Hồ Quách Lin qua số điện thoại 0987 904 586 để thanh toán gói cước và kích hoạt linsce key. " + "</p>":""}
                    <p style="margin:0 0 12px 0;font-size:16px;line-height:24px;font-family:Arial,sans-serif;">Cảm ơn bạn đã tin tưởng và sử dụng dịch vụ của <b> HQL - Revit Mep Plugins</b></p>
                    <p style="margin:0 0 12px 0;font-size:18px;line-height:24px;font-family:Arial,sans-serif; font-weight: 700; color: #3379BC;">Best regards,</p>
                  </td>
                </tr>
              
              </table>
            </td>
          </tr>
          <tr>
            <td style="padding:30px;background:#ee4c50;">
              <table role="presentation" style="width:100%;font-size:9px;font-family:Arial,sans-serif;">
                <tr>
                  <td style="padding:0;width:50%; color: #ffffff;" align="left">
                      Fanpage: https://www.facebook.com/bimsmart.hql <br /><br />
                      Hotline: 0987 904 586
                  </td>

                </tr>
              </table>
            </td>
          </tr>
        </table>
      </td>
    </tr>
  </table>
            `
    return string
}
export const  EmailOption = {
  email:"hqlbimmepsolutions@gmail.com",
  password:"ogtnkxsktevvhgzf"
}

