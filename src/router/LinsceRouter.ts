import express from 'express'
import LinsceCtrl from '../controller/Linsce/Linsce'
import { AuthMiddleware } from '../middleware/Auth'
const routerLinsce = express.Router()

routerLinsce.post('/linsce', LinsceCtrl.getLinsceKey)
routerLinsce.post('/linsce/detail', LinsceCtrl.getLinsceKeyByID)
routerLinsce.post('/linsce/create',LinsceCtrl.createLinsceKey)
routerLinsce.post('/linsce/extends',[AuthMiddleware.auth], LinsceCtrl.extendsLinsceKey)
routerLinsce.post('/linsce/verify', LinsceCtrl.verifyKey)
routerLinsce.post('/linsce/reset', LinsceCtrl.resetSerial)
routerLinsce.delete('/linsce/:id',[AuthMiddleware.auth], LinsceCtrl.deleteKey)

export default routerLinsce