import express from 'express'
import NotificationCtrl from '../controller/Notification/Notification'
import { Response, Request, NextFunction } from "express"
import ResponseConfig from "../config/Response"
import LinsceModels from "../model/LinsceKey/LinsceKey"
import AuthModels from "../model/Auth/Auth"
import NotificatioModels from "../model/Notification/Notification"
const ControlerShare = {
    fetchDBAll: async (req:Request,res:Response,next:NextFunction)=>{
        try {
            const allNotification:any = await NotificatioModels.countDocuments()
            const allLinsceModels:any = await LinsceModels.countDocuments()
            const allAuthModels:any = await AuthModels.countDocuments()
            const key_active:any = await LinsceModels.find({serial:""}).countDocuments()
            return new ResponseConfig(1000,{
                notification_count:allNotification,
                linsce_count:allLinsceModels,
                auth_count:allAuthModels,
                key_active:key_active
            },"Get all notification success.").ResponseSuccess(req,res)
        } catch (error:any) {
            return new ResponseConfig(2000,{},"Server error.").ResponseSuccess(req,res)
        }
      },
}

const routerShare = express.Router()

routerShare.post('/share/count-model', ControlerShare.fetchDBAll)


export default routerShare