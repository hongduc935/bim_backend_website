import express from 'express'
import Payments from '../controller/Payments/Payments'
const routerPayment = express.Router()

routerPayment.post('/payment', Payments.getAllPaymentByID)
routerPayment.post('/payment/create', Payments.createPayment)
export default routerPayment