import express from 'express'
import PackageCtrl from '../controller/Package/Package'
const routerPackage = express.Router()

routerPackage.post('/package', PackageCtrl.getaAllPackage)
routerPackage.post('/package/create', PackageCtrl.createPackage)
routerPackage.delete('/package/:id', PackageCtrl.deletePackage)

export default routerPackage