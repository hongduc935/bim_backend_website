import routerAuth  from  './AuthRouters'
import routerLinsce from './LinsceRouter'
import routerNotification from './Notification'
import routerShare from './ShareRouter'
import routerPayment from './PaymentRouter'
import routerPost from './PostLPRouters'
import routerSetting from './SettingRouters'
import feedbackRouters from './FeedbackRoutes'
import routerPackage from './PackageRouters'
import routerBlog from './BlogRouter'
const indexRoutes = [
    routerAuth,
    routerLinsce,
    routerNotification,
    routerShare,
    routerPayment,
    feedbackRouters,
    routerPost,
    routerSetting,
    routerPackage,
    routerBlog
]

export default indexRoutes