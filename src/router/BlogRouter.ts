import express from 'express'
import { AuthMiddleware } from "../middleware/Auth"
import BlogCtrl  from "../controller/Blog/Blog"

const routerBlog = express.Router()

// routerBlog.put('/auth/update-avatar/:id', AuthCtrl.updateAvatar)
routerBlog.post('/blog/create', BlogCtrl.create)
routerBlog.post('/blog/mac', BlogCtrl.getBlogMac)
routerBlog.post('/blog/:slug', BlogCtrl.getBlogBySlug)

export default routerBlog

