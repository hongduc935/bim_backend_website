import express from 'express'
import PostLPCtrl from '../controller/PostLP/PostLP'
const routerPost = express.Router()

routerPost.post('/post/create', PostLPCtrl.addPostLPByAdmin)
routerPost.post('/post/getAll', PostLPCtrl.getPostLPAdmin)


export default routerPost