import express from 'express'
import NotificationCtrl from '../controller/Notification/Notification'
const routerNotification = express.Router()

routerNotification.post('/notification/send-version', NotificationCtrl.sendUpdateVersion)
routerNotification.post('/notification/get-infomation', NotificationCtrl.getNotificationByUserApp)
routerNotification.post('/notification/get-all', NotificationCtrl.getNotificationAdmin)
routerNotification.post('/notification/send-exprire', NotificationCtrl.sendEmailLinsceKeyExprise)
routerNotification.delete('/notification/:id', NotificationCtrl.deleteNotificationAdmin)

export default routerNotification