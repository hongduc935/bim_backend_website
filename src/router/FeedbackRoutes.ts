import express from 'express'
import FeedBackCtrl from '../controller/Feedback/FeedBackCtrl'
const feedbackRouters = express.Router()

feedbackRouters.post('/feedback', FeedBackCtrl.getFeedBack)
// feedbackRouters.post('/linsce/detail', FeedBackCtrl.getLinsceKeyByID)
feedbackRouters.post('/feedback/create', FeedBackCtrl.createFeedBack)


export default feedbackRouters