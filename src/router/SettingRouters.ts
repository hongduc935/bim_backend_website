import express from 'express'
import SettingCtrl from '../controller/Setting/Setting'
const routerSetting = express.Router()

routerSetting.post('/setting/count-download', SettingCtrl.getCountDownload)
routerSetting.post('/setting/update-download', SettingCtrl.updateCountDownload)

export default routerSetting