export const convertDateDDMMMMYYYY = (value:any) => {
    let date = new Date(value)
    return  date.getDate()+"-"+ (date.getMonth()+1) +"-"+ date.getFullYear()
}
export const convertDateYYYYMMDD = (value:any) => {
    let date = new Date(value)
    return date.getFullYear() +"-"+ (date.getMonth()+1) +"-"+ date.getDate()
}