import { Response, Request, NextFunction } from "express"
const CryptoJS = require("crypto-js")
import ResponseConfig from "../../config/Response"
import LinsceModels from "../../model/LinsceKey/LinsceKey"
import AuthModels from "../../model/Auth/Auth"
const si = require('systeminformation')
const secretKeyHash = "abc"

const generateKey = (email:string,phone:string,serial:string)=>{
    let keyHash = CryptoJS.AES.encrypt(email+phone+serial, secretKeyHash).toString();
    return keyHash
}
const LinsceCtrl = {

    getLinsceKey:async (req:Request,res:Response,next:NextFunction)=>{
      try {
          const allLinsceKey = await LinsceModels.find({}).sort({ createdAt: -1 })
          return new ResponseConfig(1000,allLinsceKey,"Get all linsce key successed.").ResponseSuccess(req,res)
      } catch (error:any) {
          return res.status(500).json({ msg: "Server Error", error: error.message })
      }
    },
    getLinsceKeyByID:async (req:Request,res:Response,next:NextFunction)=>{
      try {
          const {id} = req.body
          const LinsceKey = await LinsceModels.findById(id)
          return new ResponseConfig(1000,LinsceKey,"Get linsce key successed.").ResponseSuccess(req,res)
      } catch (error:any) {
          return res.status(500).json({ msg: "Server Error", error: error.message })
      }
    },
    extendsLinsceKey:async (req:Request,res:Response,next:NextFunction)=>{
      try {
          const {exprire,id,status} = req.body

          console.log(status)

          let Linscekey_record:any = await LinsceModels.findById(id) 

          Linscekey_record.exprire = (Number(Linscekey_record.exprire) + Number(exprire)).toString()

          if(Number(status) !==2)
          {
            Linscekey_record.status = Number(status) === 1 ? true : false
          }
        
          await Linscekey_record.save()

          return new ResponseConfig(1000,{},"Extend linsce key successed.").ResponseSuccess(req,res)
      } catch (error:any) {
          return res.status(500).json({ msg: "Server Error", error: error.message })
      }
    },
    createLinsceKey:async (req:Request,res:Response,next:NextFunction)=>{
      try {
          const {email, name,phone,packages} = req.body

          let checkEmailDuplicate = await LinsceModels.findOne({$or: [
            { 'email': email },
            { 'phone': phone },
          ]})

          if(checkEmailDuplicate)return new ResponseConfig(2000,{},"Key Email/Phone is existed.").ResponseSuccess(req,res)
          
          checkEmailDuplicate = await AuthModels.findOne({$or: [
            { 'email': email },
            { 'phone': phone },
          ]})
          if(!checkEmailDuplicate)return new ResponseConfig(2000,{},"Please register account.").ResponseSuccess(req,res)

          let dataSystem =await si.blockDevices()
          
          let keyHash = generateKey(email,phone,dataSystem[0].serial)
          let Linsce = new LinsceModels({
            email,
            phone,
            serial:"",
            name,
            keyHash,
            exprire:packages.toString(),
            status:Number(packages) === 30 ? true : false,
          })
          await Linsce.save()
          return new ResponseConfig(1000,keyHash,"Key successfully created").ResponseSuccess(req,res)
      } catch (error:any) {
          return res.status(500).json({ msg: "Server Error", error: error.message })
      }
    },
  
    verifyKey:async (req:Request,res:Response,next:NextFunction)=>{
        try {
          const {keyHash,serial} = req.body
          if(keyHash.length === 0 || serial.length === 0 ){
              return new ResponseConfig(2000,{},"Please enter KeyLinsce/Serial Number").ResponseSuccess(req,res)
          }
          // tìm key hash
          let data_check :any=await LinsceModels.findOne({keyHash})
          let data_check_serial :any=await LinsceModels.findOne({serial})

          if(!data_check ){ // linsce key chưa tồn tại trên hệ thống
              return new ResponseConfig(2000,{},"Linsce key is not existed").ResponseSuccess(req,res)
          }

          // console.log("data_check_serial:"+data_check_serial)
          if(data_check_serial && data_check_serial.keyHash !== keyHash ){ // serial tồn tại trên hệ thống và linsce key khác linsce key hệ thống
              return new ResponseConfig(2000,{},"Device is registered linsce key . Please contact admin to suppoted. ").ResponseSuccess(req,res)
          }

          if(data_check.serial !== null && data_check.serial.length > 0){// nếu data co serial
              if(serial !== data_check.serial  ){
                return new ResponseConfig(2000,{},"Linsce đã được đăng kí trên 1 máy khác. Bạn vui lòng tạo key mới .").ResponseSuccess(req,res)
              }
              else{
                let today = new Date()
                let date = new Date(data_check.createdAt);
                let exprire = Number(data_check.exprire)
                if(today.getTime() - date.getTime() > exprire*86400000 ){
                  return new ResponseConfig(1000,{
                    exprire,
                    today,
                    date,
                  },"Hết hạn ").ResponseSuccess(req,res)
                }
                return new ResponseConfig(1000,data_check,"Verify key success. ").ResponseSuccess(req,res)
              }
          }
          else{
              if(data_check.status === false){ // nếu nó chưa được kích hoạt
                return new ResponseConfig(2000,data_check,"Linsce chưa được kích hoạt Bạn vui lòng liên hệ admin để được kích hoạt.").ResponseSuccess(req,res)
              }
              data_check.serial = serial
              data_check.status = true 
              await data_check.save()
              return new ResponseConfig(1000,data_check,"Verify key success. ").ResponseSuccess(req,res)
          }
        } catch (error:any) {
            return res.status(500).json({ msg: "Server Error", error: error.message })
        }
    },
    resetSerial:async (req:Request,res:Response,next:NextFunction)=>{
      try {
          let {keyHash} = req.body

          let data_check :any=await LinsceModels.findOne({keyHash})
          data_check.serial = ""
          data_check.status = true 
          await data_check.save()
          // let response = await LinsceModels.findByIdAndDelete(id)
          return new ResponseConfig(1000,{},"Reset Serial key success !!").ResponseSuccess(req,res)
      } catch (error:any) {
          return res.status(500).json({ msg: "Server Error", error: error.message })
      }
    },
    deleteKey:async (req:Request,res:Response,next:NextFunction)=>{
      try {
          let {id} = req.params
          let response = await LinsceModels.findByIdAndDelete(id)
          return new ResponseConfig(1000,response,"Delete key success !!").ResponseSuccess(req,res)
      } catch (error:any) {
          return res.status(500).json({ msg: "Server Error", error: error.message })
      }
    },
    deleteMultiKey:async (req:Request,res:Response,next:NextFunction)=>{
      try {
          let response = await LinsceModels.deleteMany({status:false})
          return res.status(200).json({
            msg: "Delete  key expise success !!",
            data: response
          })
      } catch (error:any) {
          return res.status(500).json({ msg: "Server Error", error: error.message })
      }
    },


}

export default LinsceCtrl