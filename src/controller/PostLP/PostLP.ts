import { Response, Request, NextFunction } from "express"
import ResponseConfig from "../../config/Response"
import PostLPModels from "../../model/Post/Post"

const PostLPCtrl = {

    getPostLPAdmin:async (req:Request,res:Response,next:NextFunction)=>{
      try {
          const allPost:any = await PostLPModels.find({}).sort({ createdAt: -1 })
          return new ResponseConfig(1000,allPost,"Get all Post success.").ResponseSuccess(req,res)
      } catch (error:any) {
          return res.status(500).json({ msg: "Server Error", error: error.message })
      }
    },

    addPostLPByAdmin:async (req:Request,res:Response,next:NextFunction)=>{
        try {
            const {title, link,short_content,description } = req.body
            let post = new PostLPModels({title, link,short_content,description})
            await post.save()
            return new ResponseConfig(1000,{},"Add Post success.").ResponseSuccess(req,res)
        } catch (error:any) {
            return res.status(500).json({ msg: "Server Error", error: error.message })
        }
    },

    deletePostLPAdmin:async (req:Request,res:Response,next:NextFunction)=>{
        try {
            let {id} = req.params
            let response:any = await PostLPModels.findByIdAndDelete(id)
            return new ResponseConfig(1000,response,"Delete key success !!").ResponseSuccess(req,res)
        } catch (error:any) {
            return res.status(500).json({ msg: "Server Error", error: error.message })
        }
    },
   


}

export default PostLPCtrl