import { Response, Request, NextFunction } from "express"
import nodemailer from 'nodemailer'
import ResponseConfig from "../../config/Response"
import LinsceModels from "../../model/LinsceKey/LinsceKey"
import {TemplateExprireNotificationEmail,TemplateUpdateVersionNotificationEmail,EmailOption} from '../../assets/Constant'
import AuthModels from "../../model/Auth/Auth"
import NotificatioModels from "../../model/Notification/Notification"


const NotificationCtrl = {

    getNotificationAdmin:async (req:Request,res:Response,next:NextFunction)=>{
      try {
          const allNotification:any = await NotificatioModels.find({}).sort({ createdAt: -1 })
          return new ResponseConfig(1000,allNotification,"Get all notification success.").ResponseSuccess(req,res)
      } catch (error:any) {
          return res.status(500).json({ msg: "Server Error", error: error.message })
      }
    },
    deleteNotificationAdmin:async (req:Request,res:Response,next:NextFunction)=>{
        try {
            let {id} = req.params
            let response:any = await NotificatioModels.findByIdAndDelete(id)
            return new ResponseConfig(1000,response,"Delete key success !!").ResponseSuccess(req,res)
        } catch (error:any) {
            return res.status(500).json({ msg: "Server Error", error: error.message })
        }
    },
    getNotificationByUserApp:async (req:Request,res:Response,next:NextFunction)=>{
        try {
            const {email,phone ,serial ,keyHash} = req.body
            let linsceKeyRecord :any = await LinsceModels.find({email,phone ,serial ,keyHash})
            if(!linsceKeyRecord){
                return new ResponseConfig(2000,{},"Không tìm thấy thông tin tài khoản.").ResponseSuccess(req,res)
            }
            let auth = await AuthModels.findOne({email,phone}).populate({
                path:"list_notification",
                options: { sort: { 'created_at': -1 } }
            }).sort({ createdAt: -1 })
            if(!auth){
                return new ResponseConfig(2000,{},"Không tìm thấy thông tin tài khoản.").ResponseSuccess(req,res)
            }
            return new ResponseConfig(1000,auth.list_notification,"Get notification user success.").ResponseSuccess(req,res)
        } catch (error:any) {
            return res.status(500).json({ msg: "Server Error", error: error.message })
        }
      },
    sendEmailLinsceKeyExprise:async (req:Request,res:Response,next:NextFunction)=>{
        try {

            const {list_user,start,end,title,content} = req.body;
            let transporter = nodemailer.createTransport({
                host: "smtp.gmail.com",
                port: 465,
                secure: false,
                service: 'gmail',
                auth: {
                    user: EmailOption.email,
                    pass: EmailOption.password
                }
            });
            let newNotification = await NotificatioModels.create({title:"Hết Hạn Linsce Key",content:"Nội dung hết hạn.",type:"exprire",list_user})
            for(const user of list_user){
                let auth:any = await AuthModels.findOne({email:user})

                await AuthModels.updateOne({email:user},{$push:{"list_notification":newNotification._id}})

                let html:string = TemplateExprireNotificationEmail({title,content,name:auth.name,start,end})
                let mailOptions:any = {
                    from: EmailOption.email,
                    to: user,
                    subject: 'LinsceKey hết hạn',
                    text:"LinsceKey hết hạn",
                    html:html
                };
                transporter.sendMail(mailOptions, function(error, info){
                    if (error) {
                        console.log(error);
                    } else {
                        console.log('Email sent: ' + info.response);
                    }
                });
            }
            return new ResponseConfig(1000,{},"Send notification exprire key successed.").ResponseSuccess(req,res)
        } catch (error:any) {
            return res.status(500).json({ msg: "Server Error", error: error.message })
        }
    },
   
    sendUpdateVersion:async (req:Request, res:Response) =>{
        try {
            let {title,content,start,end} = req.body;

            const emails = await AuthModels.find({}).select("email name")

            let transporter = nodemailer.createTransport({
                host: "smtp.gmail.com",
                port: 465,
                secure: false,
                service: 'gmail',
                auth: {
                    user: EmailOption.email,
                    pass: EmailOption.password
                }
            });
            let string_feature:string =  content;

            if(content.includes("-")){
                let string_feature_temple:string =  "";

                let list_arr:string[] = content.split("-");

                for(const x of list_arr){
                    string_feature_temple = string_feature_temple + `<p style="margin:0 0 12px 0;font-size:16px;line-height:24px;font-family:Arial,sans-serif;font-weight:bold"><i> ${x}</i> </p>`
                }

                string_feature = string_feature_temple
            }

            console.log(string_feature)

            let list_user:any[] = []
            for (const email of emails){
                list_user.push(email.email)
                let html = TemplateUpdateVersionNotificationEmail({title,content:string_feature,name:email.name,start,end,link_update:"https://www.hqlbimmepsolutions.com/"})
                let mailOptions = {
                    from: EmailOption.email,
                    to: email.email,
                    subject: title,
                    text:content,
                    html:html
                };
                transporter.sendMail(mailOptions, function(error, info){
                    if (error) {
                        console.log(error);
                    } else {
                        console.log('Email sent: ' + info.response);
                    }
                });
            }
            let newNotification = await NotificatioModels.create({title,content,type:"version",list_user})

            let result = await AuthModels.updateMany({},  { $push: { "list_notification": newNotification._id } })

            return new ResponseConfig(1000,result,"Success send email notification.").ResponseSuccess(req,res)
        } catch (error:any) {
            return res.status(500).json({ msg: "Server Error", error: error.message })
        }
    }


}

export default NotificationCtrl