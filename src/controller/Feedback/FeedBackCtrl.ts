import { Response, Request } from "express"
import ResponseConfig from "../../config/Response"
import FeedBackModels from '../../model/FeedBack/FeedBack'
import AuthModels from '../../model/Auth/Auth'
const FeedBackCtrl  = {
    getFeedBack:async (req:Request,res:Response)=>{
        try {
            const FeedBacks = await FeedBackModels.find({}).sort({ createdAt: -1 })
            return new ResponseConfig(1000,FeedBacks,"Get all Feedback key successed.").ResponseSuccess(req,res)
        } catch (error:any) {
            return res.status(500).json({ msg: "Server Error", error: error.message })
        }
    },
    createFeedBack:async (req:Request,res:Response)=>{
        try {
            const {name,email,content}= req.body
            const checkExistAccount = await AuthModels.findOne({email})
            if(!checkExistAccount){
                return new ResponseConfig(2000,{},"Bạn vui lòng đăng kí tài khoản. ").ResponseSuccess(req,res)
            }
            const FeedBacks = new  FeedBackModels({name,email,content})
            await FeedBacks.save()
            return new ResponseConfig(1000,FeedBacks,"Create Feedback key successed.").ResponseSuccess(req,res)
        } catch (error:any) {
            return res.status(500).json({ msg: "Server Error", error: error.message })
        }
    },
}

export default FeedBackCtrl