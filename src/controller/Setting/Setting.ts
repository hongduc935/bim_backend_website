import { Response, Request, NextFunction } from "express"
import ResponseConfig from "../../config/Response"
import SettingModels from "../../model/Setting/Setting"

const SettingCtrl = {

    updateCountDownload:async (req:Request,res:Response,next:NextFunction)=>{
      try {

        // Tăng số lượng total_download lên 1
        const updatedSetting = await SettingModels.findOneAndUpdate(
            {_id:"65e41852aeac339288d7e736"}, // Điều kiện tìm kiếm, để trống nếu bạn muốn cập nhật document đầu tiên trong collection
            { $inc: { total_download: 1 } }, // Thực hiện tăng giá trị
            { new: true } // Trả về document sau khi đã được cập nhật
        );

        if (updatedSetting) {
            // res.status(200).send(updatedSetting);
            return new ResponseConfig(1000,{},"Update Count Download.").ResponseSuccess(req,res)
        } else {
            // res.status(404).send({ message: 'Setting not found' });
            return new ResponseConfig(2000,{},"Server Error").ResponseSuccess(req,res)
        }

      } catch (error:any) {
          return res.status(500).json({ msg: "Server Error", error: error.message })
      }
    },

    getCountDownload:async (req:Request,res:Response,next:NextFunction)=>{
        try {
        
            // Tìm document đầu tiên (hoặc một document cụ thể dựa trên một điều kiện nếu cần)
            const setting = await SettingModels.findOne({_id:"65e41852aeac339288d7e736"}); // Bạn có thể thêm điều kiện tìm kiếm ở đây nếu cần

            if (setting) {
                return new ResponseConfig(1000,{ totalDownloads: setting.total_download },"Get Count Download").ResponseSuccess(req,res)
            } else {

                return new ResponseConfig(2000,{  },"Server Error").ResponseSuccess(req,res)
               
            }

        } catch (error:any) {
            return res.status(500).json({ msg: "Server Error", error: error.message })
        }
      },
  
   


}

export default SettingCtrl