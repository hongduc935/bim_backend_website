import { Response, Request, NextFunction } from "express"
import ResponseConfig from "../../config/Response"
import PackageModels from "../../model/Package/Package"


const PackageCtrl = {

    getaAllPackage:async (req:Request,res:Response,next:NextFunction)=>{
      try {
          const allPackage:any = await PackageModels.find({}).sort({ createdAt: -1 })
          return new ResponseConfig(1000,allPackage,"Get all Package success.").ResponseSuccess(req,res)
      } catch (error:any) {
          return res.status(500).json({ msg: "Server Error", error: error.message })
      }
    },

    createPackage:async (req:Request,res:Response,next:NextFunction)=>{
        try {

            const {name_package,price_package,date_package} = req.body

            const newPackage = new PackageModels({name_package,price_package,date_package});

            await newPackage.save();

            return new ResponseConfig(1000,{},"Create Package Success").ResponseSuccess(req,res)

        } catch (error:any) {
            return res.status(500).json({ msg: "Server Error", error: error.message })
        }
    },

    updatePackage:async (req:Request,res:Response,next:NextFunction)=>{
        try {

            const {name_package,price_package} = req.body

            const newPackage = new PackageModels({name_package,price_package});

            await newPackage.save();

            return new ResponseConfig(1000,{},"Create Package Success").ResponseSuccess(req,res)

        } catch (error:any) {
            return res.status(500).json({ msg: "Server Error", error: error.message })
        }
    },


    deletePackage:async (req:Request,res:Response,next:NextFunction)=>{
        try {
            let {id} = req.params
            let response:any = await PackageModels.findByIdAndDelete(id)
            return new ResponseConfig(1000,response,"Delete package success !!").ResponseSuccess(req,res)
        } catch (error:any) {
            return res.status(500).json({ msg: "Server Error", error: error.message })
        }
    },

 

}

export default PackageCtrl