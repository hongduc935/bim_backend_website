import { Response, Request, NextFunction } from "express"
import BlogModels from "../../model/Blog/Blog"
import ResponseConfig from "../../config/Response"

const BlogCtrl  = {

    create: async (req:Request, res:Response) => {
        try {
            const data:any = req.body

            const checkExistedSlug:any = await BlogModels.findOne({slug:data.slug})

            if(checkExistedSlug){
                return new ResponseConfig(2000,null,'Slug existed please choose other slug  ').ResponseSuccess(req,res)
            }

            const record:any = await BlogModels.create(data)

            return new ResponseConfig(1000,record,'Blog was Create Successfully ').ResponseSuccess(req,res)
        } catch (error:any) {
            return new ResponseConfig(2000,null,'Error system in [ BlogController ] : [create]' + error.message).ResponseSuccess(req,res)
        }
    },

    getBlogMac: async (req:Request, res:Response)=> {
        try {
            // const {category} = req.body
            const record:any = await BlogModels.find({})
            return new ResponseConfig(1000,record,'Blog Get All Successfully ').ResponseSuccess(req,res)
        } catch (error:any) {
            return new ResponseConfig(2000,null,'Error system in [ BlogController ] : [create]' + error.message).ResponseSuccess(req,res)
        }
    },


    getBlogBySlug: async (req:Request, res:Response) => {
        try {
            // const {slug} = req.params
            const record :any= await BlogModels.findOne({slug:req.params.slug})
            return new ResponseConfig(1000,record,'Blog Blog By Slug Successfully ').ResponseSuccess(req,res)
        } catch (error:any) {
            return new ResponseConfig(2000,null,'Error system in [ BlogController ] : [create]' + error.message).ResponseSuccess(req,res)
        }
    }

}

export default BlogCtrl